module codeberg.org/momar/linkding

go 1.22

require (
	github.com/piglig/go-qr v0.2.4-0.20240109020526-5572fa284af3
	go.etcd.io/bbolt v1.3.8
)

require golang.org/x/sys v0.4.0 // indirect

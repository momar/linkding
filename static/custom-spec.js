// Use text field as the actual spec container
function updateHidden(el) {
    if (!el || el.value === undefined) el = window.event.target;
    var customInput = document.getElementById("custom");
    if (el.value == "") {
        customInput.removeAttribute("hidden");
        customInput.value = "";
        customInput.focus();
    } else {
        customInput.setAttribute("hidden", "");
        customInput.value = el.value;
    }
}

var specSelect = document.getElementById("spec");
specSelect.addEventListener("change", updateHidden);
specSelect.removeAttribute("name");
document.getElementById("custom").name = "spec";

if (document.getElementById("spec").value == "") {
    document.getElementById("spec").value = "";
    document.getElementById("custom").removeAttribute("hidden");
}

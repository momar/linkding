package main

import (
	"bytes"
	"encoding/base64"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"codeberg.org/momar/linkding/names"
	qr "github.com/piglig/go-qr"
	"go.etcd.io/bbolt"
)

var qrImageConfig = qr.NewQrCodeImgConfig(10, 4)

func index(writer http.ResponseWriter, request *http.Request) {
	if request.URL.Query().Has("url") || request.Method == "POST" {
		createLink(writer, request)
		return
	}
	writer.Header().Set("Content-Type", "text/html; charset=utf-8")
	writer.WriteHeader(http.StatusOK)
	err := tpl.Execute(writer, templateData{
		Root:   "..",
		Domain: os.Getenv("DOMAIN"),
	})
	if err != nil {
		log.Printf("WARN: Error during tpl.Execute or writer.Write: %s", err)
	}
}

func createLink(writer http.ResponseWriter, request *http.Request) {
	displayError := func(message string, link string, spec string) {
		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		writer.WriteHeader(http.StatusBadRequest)
		err := tpl.Execute(writer, templateData{
			Root:   ".",
			Domain: os.Getenv("DOMAIN"),
			Error:  message,
			URL:    link,
			Spec:   spec,
		})
		if err != nil {
			log.Printf("WARN: Error during tpl.Execute or writer.Write: %s", err)
		}
	}

	// Parse and validate user input
	err := request.ParseForm()
	if err != nil {
		displayError("Couldn't parse form, please try again. (" + err.Error() + ")", "", "")
	}

	targetUrl := request.Form.Get("url")
	spec := request.Form.Get("spec")

	if targetUrl == "" {
		displayError("Missing or empty field \"url\", please try again.", targetUrl, spec)
	}
	if !strings.Contains(targetUrl, "://") {
		targetUrl = "https://" + targetUrl
	}
	parsedTargetUrl, err := url.Parse(targetUrl)
	if err != nil {
		displayError("Field \"url\" contains an invalid URL, please try again. (" + err.Error() + ")", targetUrl, spec)
	}
	if !parsedTargetUrl.IsAbs() {
		displayError("Field \"url\" contains an invalid URL, please try again. (must be an absolute URL)", targetUrl, spec)
	}
	if parsedTargetUrl.Scheme != "http" && parsedTargetUrl.Scheme != "https" {
		displayError("Field \"url\" contains an invalid URL, please try again. (must be a HTTP or HTTPS URL)", targetUrl, spec)
	}

	if spec == "" {
		// Fall back to default: try 3 of each kind, getting more complex as less links are available.
		spec = "#a-#n|#a-#a-#n|#a-#n-#d{4}|#a-#a-#n-#d{4}"
	}

	// Create a database transaction
	err = db.Update(func(tx *bbolt.Tx) error {
		// Validate the user input and generate a name and a token

		linkBucket := tx.Bucket([]byte("link"))
		name := names.GenerateSafe(spec, 3, func(name string) bool {
			return linkBucket.Get([]byte(name)) != nil
		})
		if name == "" {
			displayError("Custom link is already taken or invalid. Allowed characters are a-z, 0-9 and dashes (-).", targetUrl, spec)
		}
		token := names.Generate("#x{24}")

		// Write the new link to the database
		err := linkBucket.Put([]byte(name), []byte(targetUrl))
		if err != nil {
			return err
		}
		err = tx.Bucket([]byte("token")).Put([]byte(name), []byte(token))
		if err != nil {
			return err
		}
		err = tx.Bucket([]byte("count")).Put([]byte(name), []byte("0"))
		if err != nil {
			return err
		}

		redirectUrl := "../" + name + "/" + token
		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		writer.Header().Set("Location", redirectUrl)
		writer.WriteHeader(http.StatusFound)
		_, _ = writer.Write([]byte(`<!DOCTYPE html><meta http-equiv="refresh" content="0; url=` + redirectUrl + `">Redirecting to: <a href="` + redirectUrl + `">` + redirectUrl + `</a>`))
		return nil
	})
	if err != nil {
		internalError(err, writer, request)
		return
	}
}

func visitLink(writer http.ResponseWriter, request *http.Request) {
	providedLink := request.PathValue("link")
	if strings.Contains(providedLink, ".") {
		// Links must not have dots, so we have to handle a file!
		staticFileServer.ServeHTTP(writer, request)
		return
	}

	err := db.View(func(tx *bbolt.Tx) error {
		// Get the link from the database
		targetUrlBytes := tx.Bucket([]byte("link")).Get([]byte(providedLink))

		// Display a notFound error if the link doesn't exist
		if targetUrlBytes == nil || len(targetUrlBytes) == 0 {
			notFound(writer, request)
			return nil
		}

		// Count the visit
		count := func() {
			err := db.Update(func(tx *bbolt.Tx) error {
				c := tx.Bucket([]byte("count"))
				cOld, _ := strconv.Atoi(string(c.Get([]byte(providedLink))))
				return c.Put([]byte(providedLink), []byte(strconv.Itoa(cOld+1)))
			})
			if err != nil {
				log.Printf("DEBG: Couldn't count visit: %s", err)
			}
		}
		go count()

		// Redirect the user
		targetUrl := string(targetUrlBytes)
		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		writer.Header().Set("Location", targetUrl)
		writer.WriteHeader(http.StatusFound)
		_, _ = writer.Write([]byte(`<!DOCTYPE html><meta http-equiv="refresh" content="0; url=` + targetUrl + `">Redirecting to: <a href="` + targetUrl + `">` + targetUrl + `</a>`))
		return nil
	})
	if err != nil {
		internalError(err, writer, request)
		return
	}
}

func viewLink(writer http.ResponseWriter, request *http.Request) {
	providedLink := request.PathValue("link")
	providedToken := request.PathValue("token")

	// Get the link from the database
	err := db.View(func(tx *bbolt.Tx) error {
		tokenBytes := tx.Bucket([]byte("token")).Get([]byte(providedLink))
		if tokenBytes == nil {
			notFound(writer, request)
			return nil
		} else if providedToken == "" {
			// A slash is added but the token is empty, so the user probably wants to visit the link
			visitLink(writer, request)
			return nil
		} else if string(tokenBytes) != providedToken {
			writer.Header().Set("Content-Type", "text/html; charset=utf-8")
			writer.WriteHeader(http.StatusForbidden)
			return tpl.Execute(writer, templateData{
				Root:   "..",
				Domain: os.Getenv("DOMAIN"),
				Error:  "The provided edit token for this link is invalid.",
			})
		}
		targetUrl := tx.Bucket([]byte("link")).Get([]byte(providedLink))

		// Generate a QR code
		// TODO: in go.mod, move to 0.2.5 or 0.3 as soon as it's released.
		qrObject, err := qr.EncodeText(os.Getenv("DOMAIN")+"/"+providedLink, qr.Low)
		if err != nil {
			log.Printf("WARN: Couldn't encode QR code: %s", err)
		}
		qrSvgBuffer := bytes.NewBuffer([]byte{})
		err = qrObject.WriteAsSVG(qrImageConfig, qrSvgBuffer, "#fff", "#000")
		if err != nil {
			log.Printf("WARN: Couldn't convert QR code: %s", err)
		}

		// Get click count
		clicks, _ := strconv.Atoi(string(tx.Bucket([]byte("count")).Get([]byte(providedLink))))

		// Return the link view page
		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		writer.WriteHeader(http.StatusOK)
		return tpl.Execute(writer, templateData{
			Root:   "..",
			Domain: os.Getenv("DOMAIN"),
			URL:    string(targetUrl),
			Link:   providedLink,
			Token:  providedToken,
			QR:     "image/svg+xml;base64," + base64.StdEncoding.EncodeToString(qrSvgBuffer.Bytes()),
			Clicks: clicks,
		})
	})
	if err != nil {
		internalError(err, writer, request)
		return
	}
}

func deleteLink(writer http.ResponseWriter, request *http.Request) {
	providedLink := request.PathValue("link")
	providedToken := request.PathValue("token")

	// Get the link from the database
	err := db.Update(func(tx *bbolt.Tx) error {
		token := tx.Bucket([]byte("token")).Get([]byte(providedLink))
		if token == nil {
			notFound(writer, request)
			return nil
		} else if string(token) != providedToken {
			writer.Header().Set("Content-Type", "text/html; charset=utf-8")
			writer.WriteHeader(http.StatusForbidden)
			return tpl.Execute(writer, templateData{
				Root:   "..",
				Domain: os.Getenv("DOMAIN"),
				Error:  "The provided edit token for this link is invalid.",
			})
		}
		targetUrl := tx.Bucket([]byte("link")).Get([]byte(providedLink))

		err := tx.Bucket([]byte("link")).Delete([]byte(providedLink))
		if err != nil {
			return err
		}
		err = tx.Bucket([]byte("token")).Delete([]byte(providedLink))
		if err != nil {
			return err
		}
		err = tx.Bucket([]byte("count")).Delete([]byte(providedLink))
		if err != nil {
			return err
		}

		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		writer.WriteHeader(http.StatusOK)
		return tpl.Execute(writer, templateData{
			Root:    "../..",
			Domain:  os.Getenv("DOMAIN"),
			URL:     string(targetUrl),
			Spec:    providedLink,
			Message: "The link has been deleted successfully.",
		})
	})
	if err != nil {
		internalError(err, writer, request)
		return
	}
}

func listLinks(writer http.ResponseWriter, request *http.Request) {
	// TODO: not yet implemented
}

func notFound(writer http.ResponseWriter, request *http.Request) error {
	writer.Header().Set("Content-Type", "text/html; charset=utf-8")
	writer.WriteHeader(http.StatusNotFound)
	return tpl.Execute(writer, templateData{
		Root:   strings.TrimSuffix(strings.Repeat("../", strings.Count(strings.SplitN(request.URL.EscapedPath(), "?", 2)[0], "/")-1)+".", "/."),
		Domain: os.Getenv("DOMAIN"),
		Error:  "The given link doesn't exist or has been deleted.",
	})
}

func internalError(err error, writer http.ResponseWriter, request *http.Request) {
	log.Printf("FAIL: internalError: %s", err)
	writer.Header().Set("Content-Type", "text/html; charset=utf-8")
	writer.WriteHeader(http.StatusInternalServerError)
	err = tpl.Execute(writer, templateData{
		Root:   strings.TrimSuffix(strings.Repeat("../", strings.Count(strings.SplitN(request.URL.EscapedPath(), "?", 2)[0], "/")-1)+".", "/."),
		Domain: os.Getenv("DOMAIN"),
		Error:  "The server encountered an internal error.",
	})
	if err != nil {
		log.Printf("FAIL: Couldn't serve internalError message: %s", err)
	}
}

package main

type templateData struct {
	Root   string // relative path to the root, without trailing slash
	Domain string // absolute path to the root including the domain, without trailing slash

	URL  string // full link that has been shortened (or should be shortened)
	Link string // the shortened link (link page)
	Spec string // spec for the name generator (homepage)

	Token  string // edit token of the link (link page)
	QR     string // base64 data uri representation of the shortened link (link page)
	Clicks int    // number of clicks

	Error   string // error message
	Message string // success message
}

var tplString = `<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>linkding</title>
		<link rel="stylesheet" href="{{ .Root }}/bulma.min.css">
		<link rel="stylesheet" href="{{ .Root }}/style.css">
	</head>
	<body>
		<section class="hero is-fullheight" style="align-items: center">
			<div class="hero-body" style="width: 900px; max-width: 100%;">
				{{ if .Token }}<div class="container has-text-white">
					<input type="checkbox" hidden id="qr">
					<div class="input qr">
						<img src="data:{{ .QR }}" data-src="{{ .QR }}" width="100%">
					</div>
					<div class="field has-addons info">
						<div class="b control"><button onclick="document.getElementById('link').select(); document.execCommand('copy'); this.classList.add('has-background-success'); setTimeout(function(){this.classList.remove('has-background-success')}.bind(this), 450)" style="transition: background 0.5s ease-in-out" class="button is-info is-large"><img style="height: 1em" src="{{ .Root }}/copy.svg"></button></div>
						<div class="b control"><label for="qr" class="button is-large"><img style="height: 1em" src="{{ .Root }}/qr.svg"></label></div>
						<div class="control" style="flex-grow: 1"><input id="link" onfocus="this.select()" onclick="this.select()" class="input is-large" readonly value="{{ .Domain }}/{{ .Link }}"></div><script>document.getElementById("link").select();</script>
						<div class="b control"><a href="{{ .Root }}/{{ .Link }}/{{ .Token }}/delete" class="button is-danger is-large"><img style="height: 1em" src="{{ .Root }}/delete.svg"></a></div>
					</div>
					<div class="input clicks"><strong>{{ .Clicks }}</strong>&nbsp;Clicks</div>
					<!-- TODO: Button to create another link, and showing the link target -->
				</div>
				{{ else }}<form action="{{ .Root }}/manage/" method="POST" class="container">
					<div class="field has-addons">
						<input hidden id="custom" class="input" autocomplete="off" placeholder="hello-world">
						<div class="control select is-large"><select name="spec" id="spec">
							<option value="#a-#n|#a-#a-#n|#a-#n-#d{4}|#a-#a-#n-#d{3}|#a-#a-#a-#n">simplify</option>
							<option value="#x{3}|#x{4}|#x{5}|#x{6}">abbreviate</option>
							<option value="[#a-]{7}#n">lengthen</option>
							<option value="[#f-]{7}#f">faustify</option>
							<option value="" hidden="js">customize</option>
						</select></div>
						<script>
							document.querySelectorAll("option[hidden=\"js\"]").forEach(x => x.removeAttribute("hidden")); // Hide "customize" if there's no javascript enabled
							document.getElementById("spec").value = document.getElementById("custom").value = "{{ if .Spec }}{{ .Spec }}{{ else }}#a-#n|#a-#a-#n|#a-#n-#d{4}|#a-#a-#n-#d{3}|#a-#a-#a-#n{{ end }}"; // Set default spec
						</script>
						<div class="control" style="flex-grow: 1"><input name="url" class="input is-large" autocomplete="off" type="text" placeholder="https://..." value="{{ .URL }}" autofocus></div>
						<div class="control" style="flex: none"><button class="button is-info is-large"><img style="height: 1em" src="{{ .Root }}/link.svg"></button></div>
					</div>
					{{ if .Error }}<div class="error has-background-danger"><strong class="has-text-white">{{ .Error }}</strong></div>{{ end }}
					{{ if .Message }}<div class="error has-background-success"><strong class="has-text-white">{{ .Message }}</strong></div>{{ end }}
				</form>
				<script src="{{ .Root }}/custom-spec.js"></script>
				{{ end }}
			</div>
		</section>
		<!-- TODO: "My Links" sidebar with admin mode -->
		<footer>powered by <a href="https://codeberg.org/momar/linkding">linkding</a></footer>
	</body>
</html>
`

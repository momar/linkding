package main

import (
	"html/template"
	"log"
	"net/http"
	"os"

	"codeberg.org/momar/linkding/static"
	"go.etcd.io/bbolt"
)

var db *bbolt.DB
var tpl *template.Template
var staticFileServer http.Handler

func main() {
	assumeNoError := func(err error, code int) {
		if err != nil {
			log.Printf("FAIL: %s", err)
			os.Exit(code)
		}
	}

	// Initialize database
	var err error
	db, err = bbolt.Open("./database.bolt", 0666, nil)
	assumeNoError(err, 1)
	defer func(db *bbolt.DB) {
		_ = db.Close()
	}(db)
	if db == nil {
		os.Exit(2)
	}

	// Create buckets
	tx, err := db.Begin(true)
	assumeNoError(err, 1)
	for _, b := range []string{"link", "count", "token"} {
		_, err := tx.CreateBucketIfNotExists([]byte(b))
		assumeNoError(err, 1)
	}
	err = tx.Commit()
	assumeNoError(err, 1)

	// Parse template
	tpl, err = template.New("linkding").Parse(tplString)
	assumeNoError(err, 2)

	// Set up web server
	mux := http.NewServeMux()
	mux.HandleFunc("GET /{$}", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		writer.Header().Set("Location", "./manage/")
		writer.WriteHeader(http.StatusPermanentRedirect)
		_, _ = writer.Write([]byte(`<!DOCTYPE html><meta http-equiv="refresh" content="0; url=./manage/">Redirecting to: <a href="./manage/">./manage/</a>`))
	})
	mux.HandleFunc("POST /{$}", func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		writer.Header().Set("Location", "./manage/")
		writer.WriteHeader(http.StatusPermanentRedirect)
		_, _ = writer.Write([]byte(`<!DOCTYPE html><meta http-equiv="refresh" content="0; url=./manage/">Redirecting to: <a href="./manage/">./manage/</a>`))
	})
	mux.HandleFunc("GET /manage/{$}", index)
	mux.HandleFunc("POST /manage/{$}", createLink)
	mux.HandleFunc("GET /manage/my-links.json", listLinks)
	mux.HandleFunc("GET /{link}/{token}/delete", deleteLink)
	mux.HandleFunc("GET /{link}/{token}", viewLink)
	mux.HandleFunc("GET /{link}", visitLink)
	staticFileServer = http.FileServer(http.FS(static.AssetFile))
	mux.Handle("GET /{path...}", staticFileServer)

	// Start web server
	listenAddress := os.Getenv("LISTEN")
	if listenAddress == "" {
		listenAddress = ":8080"
	}
	log.Printf("INFO: Starting web server on %s", listenAddress)
	err = http.ListenAndServe(listenAddress, mux)
	assumeNoError(err, 3)
}

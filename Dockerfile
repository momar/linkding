FROM golang:1.22-alpine AS build

RUN apk --no-cache add git tzdata zip ca-certificates
COPY . /src
WORKDIR /src
RUN go mod download
RUN CGO_ENABLED=0 go build -ldflags '-s -w' -o /tmp/linkding ./cmd/linkding

WORKDIR /usr/share/zoneinfo
RUN zip -r -0 /zoneinfo.zip . >/dev/null


FROM scratch

COPY --from=build /tmp/linkding /bin/linkding
COPY --from=build /zoneinfo.zip /
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

EXPOSE 80
ENV LISTEN :80
ENV DOMAIN localhost
WORKDIR /data
CMD ["/bin/linkding"]

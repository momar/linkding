# Linkding
The linkding is a link shortener/alias service focused on awesome UX for both users and system administrators.

Awesome features include **QR code generation**, **click-to-copy**, **deleting & editing links** **click count**, and **custom link names**.

## Installation
```bash
docker run --name linkding -e DOMAIN=http://localhost -p 80:80 -v "$PWD:/data" momar/linkding
```

### Environment
- `DOMAIN` - the domain that will be used as a link prefix later, without trailing slash
- `LISTEN` - the listen address (e.g. `:80`)

### non-Docker installation
```bash
wget https://codeberg.org/momar/linkding/releases/download/v1.1.0/linkding /usr/local/bin/linkding && chmod +x /usr/local/bin/linkding
wget https://codeberg.org/momar/linkding/raw/branch/main/linkding.service -O /etc/systemd/system/linkding.service
mkdir /var/linkding && useradd -d /var/linkding -M -r linkding
vim /etc/systemd/system/linkding.service  # adjust configuration
systemctl enable --now linkding
```

You probably also want a reverse proxy like [Caddy](https://caddyserver.com/) to enable HTTPS.

## Custom names
Custom link names can contain the characters `a-z`, `0-9` and dashes (`-`). Also, there are a few placeholders for random name generation:
- `#w` is replaced by a random english word
- `#n` is replaced by a random english noun
- `#a` is replaced by a random english adjective
- `#f` is replaced by a random german word from the list of most common 1000 words in Goethe's Faust
- `#x` produces a random character (except for dashes)
- `#l` produces a random letter
- `#d` produces a random digit
- `#v` produces a random vowel
- `#c` produces a random consonant
- `#x{5}` is replaced by 5 random characters
- `[...]{5}` repeats `...` 5 times
- `[#v#c]{5}` produces 10 pronouncable characters, e.g. ``
- `#l|#x{100}` produces a single-letter link, or a 100-character link if 3 randomly generated single-letter links were all already in use

## Relevant endpoints
### http://localhost/manage/
Create a new link from the browser.

### http://localhost/manage/?spec=hello-world&url=example.org
Returns a 301 redirect to http://localhost/hello-world/abc123 - `http://localhost/hello-world` in this instance is the link that's being used, `abc123` is the edit token. `spec` is not required (see "Custom names" below), and the request can also be a POST request containing a body with those fields in [one of a couple formats](https://godoc.org/github.com/aofei/air#Request.Bind).

If the link already exists, or the URL or the spec is invalid, it returns a 400 error.

### http://localhost/hello-world
Returns a 301 redirect to (in this case) http://example.org.

### http://localhost/hello-world/abc123
Displays a page with the shortened link, a QR code, and the click count of the link.

### http://localhost/hello-world/abc123/delete
Deletes the link, so it can be created again. Is a non-idempotent GET request against all best practices, but makes the frontend way simpler, and as it can only be called once the browser-history argument is not relevant.

Displays the link creation page with all fields pre-filled to undo the deletion or to edit the link (albeit this will generate a new edit token).

If the link doesn't exist, this returns a 404 error; if the token is wrong it returns a 403 error.

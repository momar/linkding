package names

import (
	"crypto/rand"
	"math"
	"math/big"
	"regexp"
	"strconv"
	"strings"

	"codeberg.org/momar/linkding/names/words"
)

// Types maps an expansion identifier to a slice of possible values. `Generate("#x")` for example returns a random value from the `Types['x']` slice
var Types = map[byte][]string{
	'x': strings.Split("abcdefghijklmnopqrstuvwxyz0123456789", ""),
	'd': strings.Split("0123456789", ""),
	'l': strings.Split("abcdefghijklmnopqrstuvwxyz", ""),
	'v': strings.Split("aeiou", ""),
	'c': strings.Split("bcdfghjklmnpqrstvwxyz", ""),
	'w': append(words.Adjectives, words.Nouns...),
	'n': words.Nouns,
	'a': words.Adjectives,
	'f': words.Faust,
}

var MaximumLength = 1024

var specification *regexp.Regexp
var expansion *regexp.Regexp
var repetition = regexp.MustCompile(`(#?.|\[[^\]]*\])\{\d+\}`)

// GenerateSafe generates a safe name according to a set of given specs seperated by "|" by trying to generate `tries` names for each spec and checking if an item with that name already exists using the `exists` function.
func GenerateSafe(specs string, tries uint, exists func(string) bool) string {
	for _, spec := range strings.Split(specs, "|") {
		// Try a spec (from first to last)
		var i uint
		for ; i < tries; i++ {
			// Try to generate a name
			name := Generate(spec)
			if !exists(name) {
				return name
			}
		}
	}
	return ""
}

// Generate a name according to the given spec. A spec can only contain letters (a-z), numbers (0-9), dashes (-), expansions matching a type identifier from the Types map (#d) and repetitions ([text]{5} #d{5} [xyz#d]{5}). An invalid spec returns an empty string, which should be handled.
func Generate(spec string) string {
	spec = strings.TrimSpace(spec)
	if spec == "" {
		spec = "#a-#a-#n"
	}

	// Generate regular expression with type characters
	if expansion == nil {
		typeChars := ""
		for k := range Types {
			typeChars += string(k)
		}
		expansion = regexp.MustCompile(`#[` + typeChars + `]`)
		specification = regexp.MustCompile(`^([a-z0-9-]+|#[` + typeChars + `]|(\[([a-z0-9-]+|#[a-z])+\])?\{\d+\})+$`)
	}

	// Check syntax
	if !specification.MatchString(spec) {
		return ""
	}

	// Expand repetitions
	for {
		i := repetition.FindStringIndex(spec)
		if i == nil {
			break
		}

		placeholder := spec[i[0]:i[1]]
		count, _ := strconv.Atoi(placeholder[strings.LastIndex(placeholder, "{")+1 : len(placeholder)-1])
		content := strings.TrimPrefix(strings.TrimSuffix(placeholder[:strings.LastIndex(placeholder, "{")], "]"), "[")
		spec = spec[:i[0]] + strings.Repeat(content, int(math.Min(float64(count), float64(MaximumLength*2)/float64(len(content))))) + spec[i[1]:]
		if len(spec) >= MaximumLength*2 {
			break
		}
	}

	// Replace placeholders
	for {
		i := expansion.FindStringIndex(spec)
		if i == nil {
			break
		}

		placeholder := spec[i[0]:i[1]]
		words, ok := Types[placeholder[1]]
		if !ok {
			words = []string{placeholder}
		}
		n, _ := rand.Int(rand.Reader, big.NewInt(int64(len(words))))
		w := words[n.Int64()]
		spec = spec[:i[0]] + w + spec[i[1]:]
		if i[0]+len(w) >= MaximumLength {
			return spec[:MaximumLength]
		}
	}
	return spec
}
